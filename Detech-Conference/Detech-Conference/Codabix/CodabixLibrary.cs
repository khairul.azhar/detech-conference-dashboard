﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Detech_Conference.Codabix
{
    public class CodabixLibrary
    {
        public string CreateJSONWrite(string row, string node, string value)
        {
            JObject jobj = new JObject(
                    new JProperty("tk", row),
                    new JProperty("set",
                        new JArray(new JObject(
                            new JProperty("na", node),
                            new JProperty("va", value)))));

            return jobj.ToString();
        }

        public string CreateJSONRead(string row, string node)
        {
            JObject jobj = new JObject(
                    new JProperty("tk", row),
                    new JProperty("get",
                        new JArray(new JObject(
                            new JProperty("na", node)))));

            return jobj.ToString();
        }

        #region Write to codabix
        public Boolean WriteCodabixNode(string tokens, string codabixURL, string node, string value)
        {
            //this url is applied for read json file
            codabixURL = codabixURL + "/api/json";
            string[] t_arr = tokens.Split(',');

            // //Get the stream (data content) from server

            int count = 1;
            foreach (string row in t_arr)
            {
                //Create a request to the URL
                var httpRequestJson = (HttpWebRequest)WebRequest.Create(codabixURL);

                //encode all character in the specified string into sequence of bytes
                byte[] data = Encoding.UTF8.GetBytes(CreateJSONWrite(row, node, value));

                //tell the recepient the content they are dealing with
                httpRequestJson.ContentType = "application/json; charset=utf-8";
                httpRequestJson.Method = "POST";
                httpRequestJson.ContentLength = data.Length;

                //to write request data
                try
                {
                    using (Stream os = httpRequestJson.GetRequestStream())
                    {
                        os.Write(data, 0, data.Length);
                    }

                    using (HttpWebResponse responsewrite = (HttpWebResponse)httpRequestJson.GetResponse())
                    {
                        //int statuscode = responsewrite.StatusCode;

                        // Read the result.
                        JObject resultObject;
                        using (StreamReader reader = new StreamReader(responsewrite.GetResponseStream(), Encoding.UTF8))
                        {
                            string content = reader.ReadToEnd();

                            // Parse the JSON. Disable date parsing to get date-like strings in the correct format
                            using (JsonTextReader jsonReader = new JsonTextReader(new StringReader(content))
                            {
                                DateParseHandling = DateParseHandling.None
                            })
                            {
                                resultObject = JObject.Load(jsonReader);
                            }
                        }

                        // Check the result for the "set" response.
                        JObject result = (JObject)resultObject["set"]["res"];
                        int resultValue = (int)result["value"];

                        //Less than 0: Error
                        if (resultValue < 0)
                        {
                            // An error occured...
                            string errorMessage = (string)result["reason"];
                            //throw new ArgumentException(errorMessage);
                        }
                        else
                        {
                            return true;
                        }
                    }

                }
                catch (WebException ex)
                {
                    using (HttpWebResponse reswrite = (HttpWebResponse)ex.Response)
                    {
                        //StatusCode:200(StatusDescription:OK)
                    }
                }
                //Abort the Request, if not looping will shown an error
                httpRequestJson.Abort();
                count++;

            }

            return false;
        }
        #endregion

        #region Read from codabix | param: (string tokens, string codabixURL, string node)
        public List<string> ReadCodabixNode(string tokens, string codabixURL, string nodes)
        {
            List<string> values = new List<string>();
            //this url is applied for read json file
            codabixURL = codabixURL + "/api/json";
            string[] t_arr = tokens.Split(',');
            string[] n_arr = nodes.Split(',');

            // //Get the stream (data content) from server

            int count = 1;
            foreach (string row in t_arr)
            {
                foreach (string node in n_arr)
                {
                    //Create a request to the URL
                    var httpRequestJson = (HttpWebRequest)WebRequest.Create(codabixURL);

                    //encode all character in the specified string into sequence of bytes
                    byte[] data = Encoding.UTF8.GetBytes(CreateJSONRead(row, node));

                    //tell the recepient the content they are dealing with
                    httpRequestJson.ContentType = "application/json; charset=utf-8";
                    httpRequestJson.Method = "POST";
                    httpRequestJson.ContentLength = data.Length;

                    //to write request data
                    try
                    {
                        using (Stream os = httpRequestJson.GetRequestStream())
                        {
                            os.Write(data, 0, data.Length);
                        }

                        using (HttpWebResponse responsewrite = (HttpWebResponse)httpRequestJson.GetResponse())
                        {
                            //int statuscode = responsewrite.StatusCode;

                            // Read the result.
                            JObject resultObject;
                            using (StreamReader reader = new StreamReader(responsewrite.GetResponseStream(), Encoding.UTF8))
                            {
                                string content = reader.ReadToEnd();

                                // Parse the JSON. Disable date parsing to get date-like strings in the correct format
                                using (JsonTextReader jsonReader = new JsonTextReader(new StringReader(content))
                                {
                                    DateParseHandling = DateParseHandling.None
                                })
                                {
                                    resultObject = JObject.Load(jsonReader);
                                }
                            }

                            // Check the result for the "set" response.
                            JObject result = (JObject)resultObject["get"]["res"];
                            JArray resultBody = (JArray)resultObject["get"]["nodes"];
                            int resultValue = (int)result["value"];

                            //Less than 0: Error
                            if (resultValue < 0)
                            {
                                // An error occured...
                                string errorMessage = (string)result["reason"];
                                //throw new ArgumentException(errorMessage);
                            }
                            else
                            {
                                foreach (var _row in resultBody)
                                {
                                    JArray res = (JArray)_row["values"];

                                    foreach (var _res in res)
                                    {
                                        JValue value = (JValue)_res["va"];
                                        //Console.WriteLine(value.ToString());
                                        values.Add(value.ToString());
                                    }
                                }
                            }
                        }

                    }
                    catch (WebException ex)
                    {
                        using (HttpWebResponse reswrite = (HttpWebResponse)ex.Response)
                        {
                            //StatusCode:200(StatusDescription:OK)
                        }
                    }
                    //Abort the Request, if not looping will shown an error
                    httpRequestJson.Abort();
                    count++;
                }
            }

            return values;
        }
        #endregion


        
    }
}