﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Detech_Conference.Models
{
    [Table("TemperatureHistory")]
    public class TemperatureHistory
    {
        [Key]
        public Guid id_temperature_history { set; get; }
        public double value { set; get; }
        public DateTime time_record { set; get; }
    }
}