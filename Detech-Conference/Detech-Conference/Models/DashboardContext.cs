﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Detech_Conference.Models
{
    public class DashboardContext : DbContext
    {
        public DbSet<TemperatureHistory> TemperatureHistories { get; set; }
        public DbSet<MachineControl> machineControls { get; set; }
    }
}