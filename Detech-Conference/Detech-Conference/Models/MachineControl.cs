﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Detech_Conference.Models
{
    [Table("MachineControl")]
    public class MachineControl
    {
        [Key]
        public Guid id_machine_control { set; get; }
        public int plan_produce { set; get; }
        public string product_name { set; get; }
        public int ideal_cycle_time { set; get; }
        public int machine_state { set; get; }
        public DateTime time_record { set; get; }
    }
}