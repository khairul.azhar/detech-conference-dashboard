﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Detech_Conference.Class;
using Detech_Conference.Codabix;
using Detech_Conference.Models;

namespace Detech_Conference.Controllers
{
    public class ProcessManufacturingController : Controller
    {
        string codabixURL = ConfigurationManager.AppSettings["CodabixUrl"];
        string token = ConfigurationManager.AppSettings["SendToken"];
        string node = "Process State,Process Time,Total Runtime Process,Total Stop Time Process,Speed Motor 01,Speed Motor 02,Boiler Drum Pressure Value,Boiler Drum Pressure Ideal,Boiler Drum Water Level Value,Boiler Drum Water Level Ideal,Boiler Drum Pressure Alarm,Boiler Drum Water Level Alarm,Dust Level,Air Quality Value,Air Quality Ideal";
        string tokenR = ConfigurationManager.AppSettings["ReceiveToken"];

        public ActionResult dashboard()
        {
            string status = null;
            string bdPressureAlarm = null;
            string bdWaterLevelAlarm = null;
            string dustLevel = null;

            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            //set process state as string
            switch (_values[0])
            {
                case "1":
                    status = "Start";
                    break;

                case "0":
                    status = "Stop";
                    break;

                case "3":
                    status = "Alarm";
                    break;

                case "2":
                    status = "Maintenance";
                    break;
            }

            ProcessManufacturingDetails newInfo = new ProcessManufacturingDetails
            {
                ProcessState = status,
                RunTime = double.Parse(_values[2]),
                StopTime = double.Parse(_values[3]),
                SpeedMotor01 = Math.Round(double.Parse(_values[4]), 2),
                SpeedMotor02 = Math.Round(double.Parse(_values[5]), 2),
                BDPressureValue = Math.Round(double.Parse(_values[6]), 2),
                BDPressureIdeal = Math.Round(double.Parse(_values[7]), 2),
                BDWaterLevelValue = Math.Round(double.Parse(_values[8]), 2),
                AirQualityValue = double.Parse(_values[12]),
                AirQualityIdeal = double.Parse(_values[13])
            };
            
            // BD Pressure Alarm 
            switch(_values[9])
            {
                case "0":
                    bdPressureAlarm = "Low low";
                    break;

                case "1":
                    bdPressureAlarm = "Low";
                    break;

                case "2":
                    bdPressureAlarm = "Normal";
                    break;

                case "3":
                    bdPressureAlarm = "High";
                    break;

                case "4":
                    bdPressureAlarm = "High high";
                    break;
            }

            newInfo.BDPressureAlarm = bdPressureAlarm;

            // BD Water Level Alarm 
            switch (_values[10])
            {
                case "0":
                    bdWaterLevelAlarm = "Low low";
                    break;

                case "1":
                    bdWaterLevelAlarm = "Low";
                    break;

                case "2":
                    bdWaterLevelAlarm = "Normal";
                    break;

                case "3":
                    bdWaterLevelAlarm = "High";
                    break;

                case "4":
                    bdWaterLevelAlarm = "High high";
                    break;
            }

            newInfo.BDWaterLevelAlarm = bdWaterLevelAlarm;

            // Dust Level
            switch (_values[11])
            {
                case "0":
                    dustLevel = "Low low";
                    break;

                case "1":
                    dustLevel = "Low";
                    break;

                case "2":
                    dustLevel = "Normal";
                    break;

                case "3":
                    dustLevel = "High";
                    break;

                case "4":
                    dustLevel = "High high";
                    break;
            }

            newInfo.DustLevel = dustLevel;

            // Process Time
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(double.Parse(_values[1])).ToLocalTime();
            newInfo.ProcessTime = date;


            //------------------------------------------------ Start Calculation ----------------------------------------------------------------

            // Availability
            double availability = Math.Round((newInfo.RunTime - newInfo.StopTime) / (newInfo.RunTime),2);
            double availabilityPercent = availability * 100;
            if (Double.IsNaN(availabilityPercent) || Double.IsInfinity(availabilityPercent) || Double.IsNegativeInfinity(availabilityPercent))
            {
                availabilityPercent = 0;
            }
            if (availabilityPercent > 100)
            {
                availabilityPercent = 100;
            }
            newInfo.availability = availabilityPercent;

            // Performance
            double performance = newInfo.BDPressureValue / newInfo.BDPressureIdeal;
            double performancePercent = performance * 100;
            if (Double.IsNaN(performancePercent) || Double.IsInfinity(performancePercent) || Double.IsNegativeInfinity(performancePercent))
            {
                performancePercent = 0;
            }
            if (performancePercent > 100)
            {
                performancePercent = 100;
            }
            newInfo.performance = performancePercent;

            // Quality
            double quality = Math.Round((newInfo.AirQualityIdeal - newInfo.AirQualityValue) / (newInfo.AirQualityIdeal),2);
            double qualityPercent = quality * 100;
            if (Double.IsNaN(qualityPercent) || Double.IsInfinity(qualityPercent) || Double.IsNegativeInfinity(qualityPercent))
            {
                qualityPercent = 0;
            }
            if (qualityPercent > 100)
            {
                qualityPercent = 100;
            }
            newInfo.quality = qualityPercent;

            // OEE
            double oee = availability * performance * quality;
            double oeePercent = oee * 100;
            if (Double.IsNaN(oeePercent) || Double.IsInfinity(oeePercent) || Double.IsNegativeInfinity(oeePercent))
            {
                oeePercent = 0;
            }
            if (oeePercent > 100)
            {
                oeePercent = 100;
            }
            newInfo.oee = oeePercent;

            return View(newInfo);
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult BDValue()
        {
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            ProcessManufacturingDetails newInfo = new ProcessManufacturingDetails
            {
                BDPressureValue = Math.Round(double.Parse(_values[6]), 2),
                BDWaterLevelValue = Math.Round(double.Parse(_values[8]), 2)
            };

            return Json(newInfo, JsonRequestBehavior.AllowGet);

        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult oeeResult()
        {
            string status = null;
            string bdPressureAlarm = null;
            string bdWaterLevelAlarm = null;
            string dustLevel = null;

            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            //set process state as string
            switch (_values[0])
            {
                case "1":
                    status = "Start";
                    break;

                case "0":
                    status = "Stop";
                    break;

                case "3":
                    status = "Alarm";
                    break;

                case "2":
                    status = "Maintenance";
                    break;
            }

            ProcessManufacturingDetails newInfo = new ProcessManufacturingDetails
            {
                ProcessState = status,
                RunTime = double.Parse(_values[2]),
                StopTime = double.Parse(_values[3]),
                SpeedMotor01 = Math.Round(double.Parse(_values[4]), 2),
                SpeedMotor02 = Math.Round(double.Parse(_values[5]), 2),
                BDPressureValue = Math.Round(double.Parse(_values[6]), 2),
                BDPressureIdeal = Math.Round(double.Parse(_values[7]), 2),
                BDWaterLevelValue = Math.Round(double.Parse(_values[8]), 2),
                AirQualityValue = Int16.Parse(_values[12]),
                AirQualityIdeal = Int16.Parse(_values[13])
            };

            // BD Pressure Alarm 
            switch (_values[9])
            {
                case "0":
                    bdPressureAlarm = "Low low";
                    break;

                case "1":
                    bdPressureAlarm = "Low";
                    break;

                case "2":
                    bdPressureAlarm = "Normal";
                    break;

                case "3":
                    bdPressureAlarm = "High";
                    break;

                case "4":
                    bdPressureAlarm = "High high";
                    break;
            }

            newInfo.BDPressureAlarm = bdPressureAlarm;

            // BD Water Level Alarm 
            switch (_values[10])
            {
                case "0":
                    bdWaterLevelAlarm = "Low low";
                    break;

                case "1":
                    bdWaterLevelAlarm = "Low";
                    break;

                case "2":
                    bdWaterLevelAlarm = "Normal";
                    break;

                case "3":
                    bdWaterLevelAlarm = "High";
                    break;

                case "4":
                    bdWaterLevelAlarm = "High high";
                    break;
            }

            newInfo.BDWaterLevelAlarm = bdWaterLevelAlarm;

            // Dust Level
            switch (_values[11])
            {
                case "0":
                    dustLevel = "Low low";
                    break;

                case "1":
                    dustLevel = "Low";
                    break;

                case "2":
                    dustLevel = "Normal";
                    break;

                case "3":
                    dustLevel = "High";
                    break;

                case "4":
                    dustLevel = "High high";
                    break;
            }

            newInfo.DustLevel = dustLevel;

            // Process Time
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(double.Parse(_values[1])).ToLocalTime();
            newInfo.ProcessTime = date;


            //------------------------------------------------ Start Calculation ----------------------------------------------------------------

            // Availability
            double availability = (newInfo.RunTime - newInfo.StopTime) / (newInfo.RunTime);
            double availabilityPercent = availability * 100;
            if (Double.IsNaN(availabilityPercent) || Double.IsInfinity(availabilityPercent) || Double.IsNegativeInfinity(availabilityPercent))
            {
                availabilityPercent = 0;
            }
            if (availabilityPercent > 100)
            {
                availabilityPercent = 100;
            }
            newInfo.availability = availabilityPercent;

            // Performance
            double performance = newInfo.BDPressureValue / newInfo.BDPressureIdeal;
            double performancePercent = performance * 100;
            if (Double.IsNaN(performancePercent) || Double.IsInfinity(performancePercent) || Double.IsNegativeInfinity(performancePercent))
            {
                performancePercent = 0;
            }
            if (performancePercent > 100)
            {
                performancePercent = 100;
            }
            newInfo.performance = performancePercent;

            // Quality
            double quality = (newInfo.AirQualityIdeal - newInfo.AirQualityValue) / (newInfo.AirQualityIdeal);
            double qualityPercent = quality * 100;
            if (Double.IsNaN(qualityPercent) || Double.IsInfinity(qualityPercent) || Double.IsNegativeInfinity(qualityPercent))
            {
                qualityPercent = 0;
            }
            if (qualityPercent > 100)
            {
                qualityPercent = 100;
            }
            newInfo.quality = qualityPercent;

            // OEE
            double oee = availability * performance * quality;
            double oeePercent = oee * 100;
            if (Double.IsNaN(oeePercent) || Double.IsInfinity(oeePercent) || Double.IsNegativeInfinity(oeePercent))
            {
                oeePercent = 0;
            }
            if (oeePercent > 100)
            {
                oeePercent = 100;
            }
            newInfo.oee = oeePercent;

            return Json(newInfo,JsonRequestBehavior.AllowGet);
        }

        public ActionResult topInformation()
        {
            string status = null;

            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            //set process state as string
            switch (_values[0])
            {
                case "1":
                    status = "Start";
                    break;

                case "0":
                    status = "Stop";
                    break;

                case "3":
                    status = "Alarm";
                    break;

                case "2":
                    status = "Maintenance";
                    break;
            }

            ProcessManufacturingDetails newInfo = new ProcessManufacturingDetails
            {
                ProcessState = status
            };

            // Process Time
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(double.Parse(_values[1])).ToLocalTime();
            newInfo.ProcessTime = date;

            return PartialView("topInformation",newInfo);
        }

        public ActionResult botInformation()
        {
            string status = null;
            string bdPressureAlarm = null;
            string bdWaterLevelAlarm = null;
            string dustLevel = null;

            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            //set process state as string
            switch (_values[0])
            {
                case "1":
                    status = "Start";
                    break;

                case "0":
                    status = "Stop";
                    break;

                case "3":
                    status = "Alarm";
                    break;

                case "2":
                    status = "Maintenance";
                    break;
            }

            ProcessManufacturingDetails newInfo = new ProcessManufacturingDetails
            {
                RunTime = double.Parse(_values[2]),
                StopTime = double.Parse(_values[3]),
                SpeedMotor01 = Math.Round(double.Parse(_values[4]), 2),
                SpeedMotor02 = Math.Round(double.Parse(_values[5]), 2),
                BDPressureValue = Math.Round(double.Parse(_values[6]), 2),
                BDPressureIdeal = Math.Round(double.Parse(_values[7]), 2),
                BDWaterLevelValue = Math.Round(double.Parse(_values[8]), 2),
                AirQualityValue = Int16.Parse(_values[12]),
                AirQualityIdeal = Int16.Parse(_values[13])
            };

            // BD Pressure Alarm 
            switch (_values[9])
            {
                case "0":
                    bdPressureAlarm = "Low low";
                    break;

                case "1":
                    bdPressureAlarm = "Low";
                    break;

                case "2":
                    bdPressureAlarm = "Normal";
                    break;

                case "3":
                    bdPressureAlarm = "High";
                    break;

                case "4":
                    bdPressureAlarm = "High high";
                    break;
            }

            newInfo.BDPressureAlarm = bdPressureAlarm;

            // BD Water Level Alarm 
            switch (_values[10])
            {
                case "0":
                    bdWaterLevelAlarm = "Low low";
                    break;

                case "1":
                    bdWaterLevelAlarm = "Low";
                    break;

                case "2":
                    bdWaterLevelAlarm = "Normal";
                    break;

                case "3":
                    bdWaterLevelAlarm = "High";
                    break;

                case "4":
                    bdWaterLevelAlarm = "High high";
                    break;
            }

            newInfo.BDWaterLevelAlarm = bdWaterLevelAlarm;

            // Dust Level
            switch (_values[11])
            {
                case "0":
                    dustLevel = "Low low";
                    break;

                case "1":
                    dustLevel = "Low";
                    break;

                case "2":
                    dustLevel = "Normal";
                    break;

                case "3":
                    dustLevel = "High";
                    break;

                case "4":
                    dustLevel = "High high";
                    break;
            }

            newInfo.DustLevel = dustLevel;

            return PartialView("botInformation", newInfo);
        }

        // --------------------------------------------------- Machine Control --------------------------------------------------------------

        public ActionResult Index()
        {
            ViewBag.ShowDiv = false;
            return View();
        }

        public ActionResult SubmitMachineControl(bool panelSuccesses)
        {
            ViewBag.ShowDiv = panelSuccesses;
            return View();
        }

        public ActionResult airQualityIdeal(string airQualityIdeal)
        {
            CodabixLibrary codabix = new CodabixLibrary();
            bool panelSuccess = false;
            string node = null;

            node = "Air Quality Ideal";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, airQualityIdeal);

            return RedirectToAction("SubmitMachineControl", new { panelSuccesses = panelSuccess });

        }

        public ActionResult boilerDrumPressure(string boilerDrumPressure)
        {
            CodabixLibrary codabix = new CodabixLibrary();
            bool panelSuccess = false;
            string node = null;

            node = "Boiler Drum Pressure Ideal";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, boilerDrumPressure);

            return RedirectToAction("SubmitMachineControl", new { panelSuccesses = panelSuccess });

        }

        public ActionResult processState(int processState)
        {
            CodabixLibrary codabix = new CodabixLibrary();
            bool panelSuccess = false;
            string node = null;

            //update machineState node
            node = "Process State";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, processState.ToString());

            return RedirectToAction("SubmitMachineControl", new { panelSuccesses = panelSuccess });

        }

        // -------------------------------------------------------------- Machine Overview Layout ---------------------------------------------------------------


        public ActionResult Layout()
        {
            return View();
        }
    }
}