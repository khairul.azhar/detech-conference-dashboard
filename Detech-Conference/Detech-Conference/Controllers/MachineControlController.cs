﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Detech_Conference.Codabix;
using Detech_Conference.Models;

namespace Detech_Conference.Controllers
{
    public class MachineControlController : Controller
    {
        string codabixURL = ConfigurationManager.AppSettings["CodabixUrl"];
        string tokenR = ConfigurationManager.AppSettings["ReceiveToken"];
        //string token = "381:j2lF5b4SzULxnPZKtkwfNzl4cwjNWumb";
        // GET: MachineControl
        public ActionResult Index()
        {
            ViewBag.ShowDiv = false;
            return View();
        }

        public ActionResult SubmitMachineControl(bool panelSuccesses)
        {
            ViewBag.ShowDiv = panelSuccesses;
            return View();
        }

        public ActionResult ProductName(string productName)
        {
            CodabixLibrary codabix = new CodabixLibrary();
            bool panelSuccess = false;
            string node = null;

            node = "Product Name";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, productName);

            return RedirectToAction("SubmitMachineControl", new { panelSuccesses = panelSuccess });

        }

        public ActionResult CycleTime(string cycleTime)
        {
            CodabixLibrary codabix = new CodabixLibrary();
            bool panelSuccess = false;
            string node = null;

            node = "Cycle Time";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, cycleTime);

            return RedirectToAction("SubmitMachineControl", new { panelSuccesses = panelSuccess });

        }

        public ActionResult PlanTotalProduce(string planTotalProduce)
        {
            CodabixLibrary codabix = new CodabixLibrary();
            DashboardContext context = new DashboardContext();
            bool panelSuccess = false;
            string node = null;

            node = "Plan Total Produce Parts";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, planTotalProduce);
            if (panelSuccess == true)
            {
                var newProduce = new MachineControl
                {
                    id_machine_control = Guid.NewGuid(),
                    plan_produce = Int16.Parse(planTotalProduce),
                    time_record = DateTime.Now
                };

                context.machineControls.Add(newProduce);
                context.SaveChanges();
            }

            return RedirectToAction("SubmitMachineControl", new { panelSuccesses = panelSuccess });

        }

        public ActionResult MachineState(int machineState)
        {
            CodabixLibrary codabix = new CodabixLibrary();
            bool panelSuccess = false;
            string node = null;

            //update machineState node
            if (machineState == 1)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                
            }
            if (machineState == 0)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                
            }

            if (machineState == 2)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                
            }

            if (machineState == 4)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                
            }

            if (machineState == 3)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                
            }

            return RedirectToAction("SubmitMachineControl", new { panelSuccesses = panelSuccess });

        }

        //POST: MachineControl
        public ActionResult MachineControl(string productName, int cycleTime, int planTotalProduce, int machineState)
        {
            DashboardContext context = new DashboardContext();
            string node = null;
            CodabixLibrary codabix = new CodabixLibrary();
            bool panelSuccess = false;
            bool panelResult;

            //count number successful write into codabix
            int count = 0;

            // Machine Control Object
            var newMachineControl = new MachineControl
            {
                id_machine_control = Guid.NewGuid(),
                time_record = DateTime.UtcNow
            };

            //update machineState node
            if (machineState == 1)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");

                if (panelSuccess)
                {
                    count = count + 4;
                }
                newMachineControl.machine_state = 1;

            }
            if (machineState == 0)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");

                if (panelSuccess)
                {
                    count = count + 4;
                }
                newMachineControl.machine_state = 0;
            }

            if (machineState == 2)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");

                if (panelSuccess)
                {
                    count = count + 4;
                }
                newMachineControl.machine_state = 2;
            }

            if (machineState == 4)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");

                if (panelSuccess)
                {
                    count = count + 4;
                }
                newMachineControl.machine_state = 4;
            }

            if (machineState == 3)
            {
                node = "Start";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Stop";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Maintenance";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Reset";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "0");
                node = "Alarm";
                codabix.WriteCodabixNode(tokenR, codabixURL, node, "1");

                if (panelSuccess)
                {
                    count = count + 4;
                }
                newMachineControl.machine_state = 3;
            }

            if (productName != null)
            {
                node = "Product Name";
                panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, productName);
                if (panelSuccess)
                {
                    count = count + 1;
                }
                newMachineControl.product_name = productName;

            }

            node = "Cycle Time";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, cycleTime.ToString());
            if (panelSuccess)
            {
                count = count + 1;
            }
            newMachineControl.ideal_cycle_time = cycleTime;
            
            node = "Plan Total Produce Parts";
            panelSuccess = codabix.WriteCodabixNode(tokenR, codabixURL, node, planTotalProduce.ToString());
            if (panelSuccess)
            {
                count = count + 1;
            }
            newMachineControl.plan_produce = planTotalProduce;

            if (count == 7)
            {
                panelResult = true;
            }
            else
            {
                panelResult = false;
            }

            return RedirectToAction("SubmitMachineControl", new { panelSuccess = panelResult });
        }
    }
}
