﻿using Detech_Conference.Class;
using Detech_Conference.Codabix;
using Detech_Conference.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Detech_Conference.Controllers
{
    

    public class MachineStateController : Controller
    {
        string codabixURL = "http://192.168.10.11:8181";
        string token = "213:0gnHBGaNNjF4e2PP1cof+QZk4WoWYlH8";
        string node = "Run Time,Stop Time,Alarm time,Maintenance time";

        // GET: MachineState
        public ActionResult Index()
        {
            DashboardContext context = new DashboardContext();
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            if (_values.Count != 0)
            {
                Details newInfo = new Details
                {
                    RunTime = Math.Round(double.Parse(_values[0]) / 60, 2),
                    StopTime = Math.Round(double.Parse(_values[1]) / 60, 2),
                    AlarmTime = Math.Round(double.Parse(_values[2]) / 60, 2),
                    MaintenanceTime = Math.Round(double.Parse(_values[3]) / 60, 2)
                };

                ViewBag.state = newInfo;

                return View();
            }
            else
                return RedirectToAction("NoRecord");
            
        }

        //No record from codabix
        public ActionResult NoRecord()
        {
            return View();
        }
        
    }
}
