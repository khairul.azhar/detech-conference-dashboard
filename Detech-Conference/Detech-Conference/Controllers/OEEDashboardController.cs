﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Detech_Conference.Class;
using Detech_Conference.Codabix;
using Detech_Conference.Models;

namespace Detech_Conference.Controllers
{


    public class OEEDashboardController : Controller
    {
        string codabixURL = ConfigurationManager.AppSettings["CodabixUrl"];
        string token = ConfigurationManager.AppSettings["SendToken"];
        //string node = "Machine state,Product name,Run Time,Stop Time,Temperature value,Good parts,Bad parts,Alarm time,Maintenance time,Ideal Cycle time,Plan total Produce,Machine start time";
        string node = "Machine state,Product name,Total Runtime,Total Stop Time,PT100 value,Good parts,Bad parts,Total Alarm Time,Total Maintenance,Ideal Cycle time,Plan total Produce Parts,Machine start time";

        // GET: OEEDashboard
        [OutputCache(NoStore = true, Location = System.Web.UI.OutputCacheLocation.Client, Duration = 5)]
        public ActionResult Dashboard()
        {
            // refresh page every 30 seconds
            //HttpContext.Response.Headers.Add("refresh", "30; url=" + Url.Action("dashboard"));
            DashboardContext context = new DashboardContext();
            string status = null;
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            if (_values.Count != 0)
            {
                //set status machine in string
                switch (_values[0])
                {
                    case "1":
                        status = "Start";
                        break;

                    case "0":
                        status = "Stop";
                        break;

                    case "3":
                        status = "Alarm";
                        break;

                    case "2":
                        status = "Maintenance";
                        break;
                }

                //Get value from codabix
                //all time value in min
                try
                {
                    var planProduce = context.machineControls.OrderByDescending(x => x.time_record).FirstOrDefault();

                    Details newInfo = new Details
                    {
                        MachineState = status,
                        ProductName = _values[1],
                        RunTime = double.Parse(_values[2]),
                        StopTime = Math.Round(double.Parse(_values[3]), 2),
                        TemperatureValue = double.Parse(_values[4]),
                        GoodParts = Int16.Parse(_values[5]),
                        BadParts = Int16.Parse(_values[6]),
                        AlarmTime = Math.Round(double.Parse(_values[7]), 2),
                        MaintenanceTime = Math.Round(double.Parse(_values[8]), 2),
                        IdealCycleTime = Math.Round(double.Parse(_values[9]) / 60, 2),
                        PlanTotalProduced = planProduce.plan_produce

                    };

                    //convert unix timestamp to datetime
                    //System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    //dtDateTime.AddMilliseconds(3000);
                    //dtDateTime = dtDateTime.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();


                    //newInfo.MachineStateTime = TimeZoneInfo.ConvertTimeToUtc(dtDateTime);
                    DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    DateTime date = start.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();
                    newInfo.MachineStateTime = date;

                    newInfo.ProducedParts = newInfo.GoodParts + newInfo.BadParts;



                    //Temperature into database
                    var newTemperature = new TemperatureHistory
                    {
                        id_temperature_history = Guid.NewGuid(),
                        value = newInfo.TemperatureValue,
                        time_record = DateTime.Now
                    };

                    context.TemperatureHistories.Add(newTemperature);
                    context.SaveChanges();


                    //Quantity Part
                    double percentagePart = 0;
                    double totalProduce = newInfo.GoodParts + newInfo.BadParts;
                    double cal1 = (totalProduce / newInfo.PlanTotalProduced);
                    percentagePart = cal1 * 100;

                    if (Double.IsNaN(percentagePart))
                    {
                        percentagePart = 0;
                    }

                    newInfo.quantityPercentage = percentagePart;

                    //Start OEE Formula

                    //Quality
                    double qualityPercent = Convert.ToDouble(0);
                    double quality = Math.Round((newInfo.GoodParts / totalProduce), 1);
                    qualityPercent = quality * 100;
                    if (Double.IsNaN(qualityPercent) || Double.IsInfinity(qualityPercent) || Double.IsNegativeInfinity(qualityPercent))
                    {
                        qualityPercent = 0;
                    }
                    if (qualityPercent > 100)
                    {
                        qualityPercent = 100;
                        quality = 100;
                    }
                    newInfo.quality = qualityPercent;

                    //availability
                    double availabilityPercent = 0;
                    double unplannedDowntime = (newInfo.AlarmTime + newInfo.StopTime);
                    double availability = Math.Round((newInfo.RunTime - unplannedDowntime) / newInfo.RunTime, 1);
                    availabilityPercent = availability * 100;
                    if (Double.IsNaN(availabilityPercent) || Double.IsInfinity(availabilityPercent))
                    {
                        availabilityPercent = 0;
                    }
                    if (availabilityPercent > 100 )
                    {
                        availabilityPercent = 100;
                        availability = 100;
                    }
                    if(availabilityPercent < 0)
                    {
                        availabilityPercent = 0;
                        availability = 0;
                    }
                    newInfo.availability = availabilityPercent;



                    //Performance
                    double performancePercent = 0;
                    double operatingTime = Math.Round((newInfo.RunTime), 2);
                    double performance = Math.Round((totalProduce * newInfo.RunTime) / (newInfo.PlanTotalProduced*newInfo.RunTime), 1);
                    //double performance = Math.Round((totalProduce*newInfo.RunTime) / (newInfo.RunTime - unplannedDowntime), 1);
                    performancePercent = performance * 100;
                    if (Double.IsNaN(performancePercent) || Double.IsInfinity(performancePercent))
                    {
                        performancePercent = 0;
                    }
                    if(performancePercent > 101)
                    {
                        performancePercent = 100;
                        performance = 100;
                    }
                    newInfo.performance = performancePercent;
                    
                    //OEE
                    double oee = quality * performance * availability;
                    double oeePercent = oee * 100;
                    if (Double.IsNaN(oeePercent))
                    {
                        oeePercent = 0;
                    }
                    if (oeePercent > 100)
                    {
                        oeePercent = 100;
                    }
                    if (oeePercent < 0)
                    {
                        oeePercent = 0;
                    }
                    newInfo.oee = Math.Round(oeePercent, 1);
                    

                    //Get Temperature Value
                    List<TemperatureHistory> temperature = new List<TemperatureHistory>();
                    DateTime dateBefore = newInfo.MachineStateTime;
                    temperature = context.TemperatureHistories.Where(x => x.time_record > dateBefore).OrderByDescending(x => x.time_record).Take(8).ToList();

                    newInfo.temperatureValue = temperature;

                    return View(newInfo);


            }
                catch (Exception i)
            {
                return RedirectToAction("NoRecord");

            }


        }
            else
                return RedirectToAction("NoRecord");
        }

        // No record from Codabix
        public ActionResult NoRecord()
        {
            return View();
        }

        public ActionResult TopInformation()
        {
            DashboardContext context = new DashboardContext();
            string status = null;
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);
            
                //set status machine in string
                switch (_values[0])
                {
                    case "1":
                        status = "Start";
                        break;

                    case "0":
                        status = "Stop";
                        break;

                    case "3":
                        status = "Alarm";
                        break;

                    case "2":
                        status = "Maintenance";
                        break;

                    case "4":
                        status = "Reset";
                        break;
                }

               
                    var machine = context.machineControls.OrderByDescending(x => x.time_record).FirstOrDefault();

                    Details newInfo = new Details
                    {
                        MachineState = status,
                        ProductName = _values[1],
                        RunTime = double.Parse(_values[2]),
                        StopTime = Math.Round(double.Parse(_values[3]), 2),
                        TemperatureValue = double.Parse(_values[4]),
                        GoodParts = Int16.Parse(_values[5]),
                        BadParts = Int16.Parse(_values[6]),
                        AlarmTime = Math.Round(double.Parse(_values[7]), 2),
                        MaintenanceTime = Math.Round(double.Parse(_values[8]), 2),
                        IdealCycleTime = Math.Round(double.Parse(_values[9]) / 60, 2),
                        PlanTotalProduced = machine.plan_produce

                    };

            //convert unix timestamp to datetime
            //System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            //dtDateTime.AddMilliseconds(3000);
            //dtDateTime = dtDateTime.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();
            //newInfo.MachineStateTime = TimeZoneInfo.ConvertTimeToUtc(dtDateTime);
            //return PartialView("TopInformation", newInfo);

            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();
            newInfo.MachineStateTime = date;
            return PartialView("TopInformation", newInfo);
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult oeeResult()
        {
            DashboardContext context = new DashboardContext();
            string status = null;
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);
            
                    var machine = context.machineControls.OrderByDescending(x => x.time_record).FirstOrDefault();

                    Details newInfo = new Details
                    {
                        MachineState = status,
                        ProductName = _values[1],
                        RunTime = double.Parse(_values[2]),
                        StopTime = Math.Round(double.Parse(_values[3]), 2),
                        TemperatureValue = double.Parse(_values[4]),
                        GoodParts = Int16.Parse(_values[5]),
                        BadParts = Int16.Parse(_values[6]),
                        AlarmTime = Math.Round(double.Parse(_values[7]), 2),
                        MaintenanceTime = Math.Round(double.Parse(_values[8]), 2),
                        IdealCycleTime = Math.Round(double.Parse(_values[9]) / 60, 2),
                        PlanTotalProduced = machine.plan_produce

                    };

                    //convert unix timestamp to datetime
                    //System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    //dtDateTime.AddMilliseconds(3000);
                    //dtDateTime = dtDateTime.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();
                    //newInfo.MachineStateTime = TimeZoneInfo.ConvertTimeToUtc(dtDateTime);
                    DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    DateTime date = start.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();
                    newInfo.MachineStateTime = date;

                    newInfo.ProducedParts = newInfo.GoodParts + newInfo.BadParts;
            
                    double totalProduce = newInfo.GoodParts + newInfo.BadParts;
                    
                    //Start OEE Formula

                    //Quality
                    double qualityPercent = Convert.ToDouble(0);
                    double quality = Math.Round((newInfo.GoodParts / totalProduce), 1);
                    qualityPercent = quality * 100;
                    if (Double.IsNaN(qualityPercent) || Double.IsInfinity(qualityPercent) || Double.IsNegativeInfinity(qualityPercent))
                    {
                        qualityPercent = 0;
                    }
                    if (qualityPercent > 100)
                    {
                        qualityPercent = 100;
                        quality = 100;
                    }
                    newInfo.quality = qualityPercent;

                    //availability
                    double availabilityPercent = 0;
                    double unplannedDowntime = (newInfo.AlarmTime + newInfo.StopTime);
                    double availability = Math.Round((newInfo.RunTime - unplannedDowntime) / newInfo.RunTime, 1);
                    availabilityPercent = availability * 100;
                    if (Double.IsNaN(availabilityPercent) || Double.IsInfinity(availabilityPercent))
                    {
                        availabilityPercent = 0;
                    }
                    if (availabilityPercent > 100)
                    {
                        availabilityPercent = 100;
                        availability = 100;
                    }
                    if (availabilityPercent < 0)
                    {
                        availabilityPercent = 0;
                        availability = 0;
                    }
                    newInfo.availability = availabilityPercent;


                    //Performance
                    double performancePercent = 0;
                    double operatingTime = Math.Round((newInfo.RunTime), 2);
                    double performance = Math.Round((totalProduce * newInfo.RunTime) / (newInfo.PlanTotalProduced * newInfo.RunTime), 1);
                    //double performance = Math.Round((totalProduce * newInfo.RunTime) / (newInfo.RunTime - unplannedDowntime), 1);
                    performancePercent = performance * 100;
                    if (Double.IsNaN(performancePercent) || Double.IsInfinity(performancePercent))
                    {
                        performancePercent = 0;
                    }
                    if (performancePercent > 101)
                    {
                        performancePercent = 100;
                        performance = 100;
                    }
                    newInfo.performance = performancePercent;


                    //OEE
                    double oee = quality * performance * availability;
                    double oeePercent = oee * 100;
                    if (Double.IsNaN(oeePercent))
                    {
                        oeePercent = 0;
                    }
                    if (oeePercent > 100)
                    {
                        oeePercent = 100;
                    }
                    if (oeePercent < 0)
                    {
                        oeePercent = 0;
                    }
                    newInfo.oee = Math.Round(oeePercent, 1);
            
                    return Json( newInfo,JsonRequestBehavior.AllowGet);
              


        }

        public ActionResult quantity()
        {
            // refresh page every 30 seconds
            //HttpContext.Response.Headers.Add("refresh", "30; url=" + Url.Action("dashboard"));
            DashboardContext context = new DashboardContext();
            string status = null;
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);
            
                //Get value from codabix
                //all time value in min
                
                    var planProduce = context.machineControls.OrderByDescending(x => x.time_record).FirstOrDefault();

                    Details newInfo = new Details
                    {
                        MachineState = status,
                        ProductName = _values[1],
                        RunTime = double.Parse(_values[2]),
                        StopTime = Math.Round(double.Parse(_values[3]), 2),
                        TemperatureValue = double.Parse(_values[4]),
                        GoodParts = Int16.Parse(_values[5]),
                        BadParts = Int16.Parse(_values[6]),
                        AlarmTime = Math.Round(double.Parse(_values[7]), 2),
                        MaintenanceTime = Math.Round(double.Parse(_values[8]), 2),
                        IdealCycleTime = Math.Round(double.Parse(_values[9]) / 60, 2),
                        PlanTotalProduced = planProduce.plan_produce

                    };
                    newInfo.ProducedParts = newInfo.GoodParts + newInfo.BadParts;
            //Quantity Part
            double percentagePart = 0;
            double totalProduce = newInfo.GoodParts + newInfo.BadParts;
            double cal1 = (totalProduce / newInfo.PlanTotalProduced);
            percentagePart = cal1 * 100;

            if (Double.IsNaN(percentagePart))
            {
                percentagePart = 0;
            }

            newInfo.quantityPercentage = percentagePart;

            return PartialView("quantity", newInfo);


            
        }

        public ActionResult BotInformation()
        {
            // refresh page every 30 seconds
            //HttpContext.Response.Headers.Add("refresh", "30; url=" + Url.Action("dashboard"));
            DashboardContext context = new DashboardContext();
            string status = null;
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);
            
            var planProduce = context.machineControls.OrderByDescending(x => x.time_record).FirstOrDefault();

            Details newInfo = new Details
            {
                MachineState = status,
                ProductName = _values[1],
                RunTime = double.Parse(_values[2]),
                StopTime = Math.Round(double.Parse(_values[3]), 2),
                TemperatureValue = double.Parse(_values[4]),
                GoodParts = Int16.Parse(_values[5]),
                BadParts = Int16.Parse(_values[6]),
                AlarmTime = Math.Round(double.Parse(_values[7]), 2),
                MaintenanceTime = Math.Round(double.Parse(_values[8]), 2),
                IdealCycleTime = Math.Round(double.Parse(_values[9]) / 60, 2),
                PlanTotalProduced = planProduce.plan_produce

            };
            
            return PartialView("BotInformation", newInfo);



        }
        
        public ActionResult TemperatureBotInformation()
        {
            DashboardContext context = new DashboardContext();
            string status = null;
            List<string> _values = new List<string>();
            CodabixLibrary codabix = new CodabixLibrary();
            _values = codabix.ReadCodabixNode(token, codabixURL, node);

            
                    var planProduce = context.machineControls.OrderByDescending(x => x.time_record).FirstOrDefault();

                    Details newInfo = new Details
                    {
                        MachineState = status,
                        ProductName = _values[1],
                        RunTime = double.Parse(_values[2]),
                        StopTime = Math.Round(double.Parse(_values[3]), 2),
                        TemperatureValue = double.Parse(_values[4]),
                        GoodParts = Int16.Parse(_values[5]),
                        BadParts = Int16.Parse(_values[6]),
                        AlarmTime = Math.Round(double.Parse(_values[7]), 2),
                        MaintenanceTime = Math.Round(double.Parse(_values[8]), 2),
                        IdealCycleTime = Math.Round(double.Parse(_values[9]) / 60, 2),
                        PlanTotalProduced = planProduce.plan_produce

                    };

                    //convert unix timestamp to datetime
                    //System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    //dtDateTime.AddMilliseconds(3000);
                    //dtDateTime = dtDateTime.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();


                    //newInfo.MachineStateTime = TimeZoneInfo.ConvertTimeToUtc(dtDateTime);
                    DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    DateTime date = start.AddMilliseconds(double.Parse(_values[11])).ToLocalTime();
                    newInfo.MachineStateTime = date;

                    newInfo.ProducedParts = newInfo.GoodParts + newInfo.BadParts;



                    //Temperature into database
                    var newTemperature = new TemperatureHistory
                    {
                        id_temperature_history = Guid.NewGuid(),
                        value = newInfo.TemperatureValue,
                        time_record = DateTime.Now
                    };

                    context.TemperatureHistories.Add(newTemperature);
                    context.SaveChanges();


                    //Quantity Part
                    double percentagePart = 0;
                    double totalProduce = newInfo.GoodParts + newInfo.BadParts;
                    double cal1 = (totalProduce / newInfo.PlanTotalProduced);
                    percentagePart = cal1 * 100;

                    if (Double.IsNaN(percentagePart))
                    {
                        percentagePart = 0;
                    }

                    newInfo.quantityPercentage = percentagePart;
            
                    //Get Temperature Value
                    List<TemperatureHistory> temperature = new List<TemperatureHistory>();
                    DateTime dateBefore = newInfo.MachineStateTime;
                    temperature = context.TemperatureHistories.Where(x => x.time_record > dateBefore).OrderByDescending(x => x.time_record).Take(8).ToList();

                    newInfo.temperatureValue = temperature;

                    return PartialView("TemperatureBotInformation", newInfo);
            
        }

    }
}
