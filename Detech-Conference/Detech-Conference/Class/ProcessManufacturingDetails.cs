﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Detech_Conference.Class
{
    public class ProcessManufacturingDetails
    {
        public string ProcessState { set; get; }
        public DateTime ProcessTime { set; get; }
        public double RunTime { set; get; }
        public double StopTime { set; get; }
        public double SpeedMotor01 { set; get; }
        public double SpeedMotor02 { set; get; }
        public double BDPressureValue { set; get; }
        public double BDPressureIdeal { set; get; }
        public double BDWaterLevelValue { set; get; }
        public string BDPressureAlarm { set; get; }
        public string BDWaterLevelAlarm { set; get; }
        public string DustLevel { set; get; }
        public double AirQualityValue { set; get; }
        public double AirQualityIdeal { set; get; }
        public double performance { set; get; }
        public double availability { set; get; }
        public double quality { set; get; }
        public double oee { set; get; }
    }
}