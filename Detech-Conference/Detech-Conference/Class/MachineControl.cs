﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Detech_Conference.Class
{
    public class MachineControl
    {
        public string productName { set; get; } 
        public double cycleTime { set; get; }
        public int planTotalProduct { set; get; }
        public int machineState { set; get; }
    }
}