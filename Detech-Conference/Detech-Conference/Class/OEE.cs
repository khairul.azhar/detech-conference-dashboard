﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Detech_Conference.Class
{
    public class OEE
    {
        public double quality { set; get; }
        public double performance { set; get; }
        public double availability { set; get; }    
        public double oee { set; get; }
    }
}