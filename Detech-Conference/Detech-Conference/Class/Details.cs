﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Detech_Conference.Class
{
    public class Details
    {

        public string MachineState { set; get; }
        public string ProductName { set; get; }
        public double RunTime { set; get; }
        public double StopTime { set; get; }
        public double TemperatureValue { set; get; }
        public int GoodParts { set; get; }
        public int BadParts { set; get; }
        public double AlarmTime { set; get; }
        public double MaintenanceTime { set; get; }
        public int ProducedParts { set; get; }
        public double IdealCycleTime { set; get; }
        public int PlanTotalProduced { set; get; }
        public DateTime MachineStateTime { set; get; }
        public double quality { set; get; }
        public double performance { set; get; }
        public double availability { set; get; }
        public double oee { set; get; }
        public double quantityPercentage { set; get; }
        public object temperatureValue { set; get; }

    }
}